package sn.gestionTache.servlet;

import sn.gestionTache.domain.Tache;
import sn.gestionTache.operation.TacheOperation;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TaskServlet extends HttpServlet {

    private final TacheOperation tacheOperation = new TacheOperation();

    // Méthode pour gérer les requêtes HTTP GET (afficher la liste des tâches)
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Récupérer la liste des tâches depuis la base de données
        List<Tache> taches = tacheOperation.listTache();

        // Mettre la liste des tâches dans l'objet request pour l'affichage dans la JSP
        request.setAttribute("tasks", taches);

        // Rediriger vers la page JSP qui affiche la liste des tâches
        RequestDispatcher dispatcher = request.getRequestDispatcher("/taches.jsp");
        dispatcher.forward(request, response);
    }

    // Méthode pour gérer les requêtes HTTP POST (ajouter/modifier/supprimer une tâche)
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/create")) {
            // Récupérer les paramètres du formulaire pour créer une nouvelle tâche
            String titre = request.getParameter("titre");
            String description = request.getParameter("description");

            String dateEcheanceParam = request.getParameter("dateEcheance");
            Date dateEcheance = null;
            try {
                dateEcheance = new SimpleDateFormat("yyyy-MM-dd").parse(dateEcheanceParam);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            String statusParam = request.getParameter("statut");
            boolean statut = (statusParam != null) && Boolean.parseBoolean(statusParam);

            // Créer une nouvelle instance de Task avec les paramètres
            Tache newTask = new Tache(titre, description, statut, dateEcheance);

            // Enregistrer la nouvelle tâche dans la base de données
            tacheOperation.createTache(newTask);

            // Rediriger vers la page qui affiche la liste mise à jour des tâches
            response.sendRedirect(request.getContextPath() + "/taches");
        } else if (pathInfo.startsWith("/edit")) {
            // Récupérer l'identifiant de la tâche et le nouveau statut depuis la requête
            Long taskId = Long.parseLong(request.getParameter("taskId"));
            boolean newStatus = Boolean.parseBoolean(request.getParameter("statut"));

            // Mettre à jour le statut de la tâche dans la base de données
            tacheOperation.updateOrDeleteTacheById(taskId, newStatus);

            // Retourner une réponse appropriée
            response.setStatus(HttpServletResponse.SC_OK);
            response.sendRedirect(request.getContextPath() + "/taches");
        } else if (pathInfo.startsWith("/delete")) {
            // Récupérer l'identifiant de la tâche depuis la requête
            Long taskId = Long.parseLong(request.getParameter("taskId"));

            // Supprimer la tâche de la base de données
            tacheOperation.updateOrDeleteTacheById(taskId, null);

            // Retourner une réponse appropriée
            response.setStatus(HttpServletResponse.SC_OK);
            response.sendRedirect(request.getContextPath() + "/taches");
        } else {
            // Handle other cases, possibly show a 404 page
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
