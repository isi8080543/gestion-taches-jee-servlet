package sn.gestionTache.servlet;

import sn.gestionTache.domain.Tache;
import sn.gestionTache.operation.TacheOperation;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.IOException;
import java.util.List;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {

    private final TacheOperation tacheOperation = new TacheOperation();

    // Méthode pour gérer les requêtes HTTP GET (afficher la liste des tâches)
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Récupérer la liste des tâches depuis la source de données
        List<Tache> tasks = tacheOperation.listTache();

        // Définir l'attribut des tâches dans la portée de la requête
        request.setAttribute("tasks", tasks);

        // Rediriger la requête vers index.jsp
        RequestDispatcher dispatcher = request.getRequestDispatcher("/taches.jsp");
        dispatcher.forward(request, response);
    }
}
