package sn.gestionTache.operation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import sn.gestionTache.domain.Tache;

public class TacheOperation {

	private static final String persistenceUnitName = "myPersistenceUnit";
	private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);

	// Méthode pour créer une nouvelle tâche
	public void createTache(Tache tache) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entTransaction = entityManager.getTransaction();

		try {
			entTransaction.begin();
			entityManager.persist(tache);
			entTransaction.commit();
		} catch (Exception e) {
			if (entTransaction != null && entTransaction.isActive()) {
				entTransaction.rollback();
			}
			e.printStackTrace();
			throw new RuntimeException("Échec de la création de la tâche.", e);
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

	// Méthode pour lister toutes les tâches
	public List<Tache> listTache() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		try {
			Query query = entityManager.createQuery("SELECT t FROM Tache t");
			List<Tache> tasks = query.getResultList();
			return tasks;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Échec de la récupération des tâches.", e);
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

	// Méthode pour mettre à jour ou supprimer une tâche par ID
	public void updateOrDeleteTacheById(Long id, Boolean newStatus) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entTransaction = entityManager.getTransaction();

		try {
			entTransaction.begin();
			Tache task = entityManager.find(Tache.class, id);

			if (task != null) {
				if (newStatus != null) {
					// Si newStatus n'est pas null, mettre à jour le statut
					task.setStatut(newStatus);
				} else {
					// Si newStatus est null, supprimer la tâche
					entityManager.remove(task);
				}
			}

			entTransaction.commit();
		} catch (Exception e) {
			if (entTransaction != null && entTransaction.isActive()) {
				entTransaction.rollback();
			}
			e.printStackTrace();
			throw new RuntimeException("Échec de la mise à jour ou de la suppression de la tâche.", e);
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

}
