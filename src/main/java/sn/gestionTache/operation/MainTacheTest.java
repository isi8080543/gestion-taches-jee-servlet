package sn.gestionTache.operation;
import java.util.Date;
import sn.gestionTache.domain.Tache;


public class MainTacheTest {
	public static void main(String[] args) {
		TacheOperation tacheOperation = new TacheOperation();

		// Test de createTache
		Tache newTask = new Tache("Test Task", "Description", false, new Date());
		tacheOperation.createTache(newTask);
		System.out.println("Tâche créée avec succès. ID : " + newTask.getId());

		// Test de listTache
		System.out.println("Liste des tâches :");
		tacheOperation.listTache().forEach(task -> System.out.println(task.getTitre()));

		// Test de updateOrDeleteTacheById (mise à jour)
		Tache taskToUpdate = tacheOperation.listTache().get(0); // En supposant qu'il y a au moins une tâche
		taskToUpdate.setStatut(true);
		tacheOperation.updateOrDeleteTacheById(taskToUpdate.getId(), taskToUpdate.getStatut());
		System.out.println("Tâche mise à jour avec succès. ID : " + taskToUpdate.getId());

		// Test de updateOrDeleteTacheById (suppression)
		long taskIdToDelete = taskToUpdate.getId();
		tacheOperation.updateOrDeleteTacheById(taskIdToDelete, null);
		System.out.println("Tâche supprimée avec succès. ID : " + taskIdToDelete);

		// Vérification de la liste après la suppression
		System.out.println("Liste des tâches après la suppression :");
		tacheOperation.listTache().forEach(task -> System.out.println(task.getTitre()));
	}
}
