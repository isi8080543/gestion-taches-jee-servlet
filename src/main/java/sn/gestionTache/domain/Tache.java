package sn.gestionTache.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
public class Tache implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	private String titre;
	private String description;
	private boolean statut;
	private Date dateEcheance;

	// Constructeur par défaut requis par JPA
	public Tache() {

	}

	// Constructeur
	public Tache(String titre, String description, boolean statut, Date dateEcheance) {
		this.titre = titre;
		this.description = description;
		this.statut = statut;
		this.dateEcheance = dateEcheance;
	}

	// Méthode d'accès pour l'ID de la tâche
	public Long getId() {
		return Id;
	}

	// Méthode de modification pour l'ID de la tâche
	public void setId(Long id) {
		Id = id;
	}

	// Méthode d'accès pour le titre de la tâche
	public String getTitre() {
		return titre;
	}

	// Méthode de modification pour le titre de la tâche
	public void setTitre(String titre) {
		this.titre = titre;
	}

	// Méthode d'accès pour la description de la tâche
	public String getDescription() {
		return description;
	}

	// Méthode de modification pour la description de la tâche
	public void setDescription(String description) {
		this.description = description;
	}

	// Méthode d'accès pour le statut de la tâche
	public boolean isStatut() {
		return statut;
	}

	// Méthode d'accès pour le statut de la tâche
	public boolean getStatut() {
		return this.statut;
	}

	// Méthode de modification pour le statut de la tâche
	public void setStatut(boolean statut) {
		this.statut = statut;
	}

	// Méthode d'accès pour la date d'échéance de la tâche
	public Date getDateEcheance() {
		return dateEcheance;
	}

	// Méthode de modification pour la date d'échéance de la tâche
	public void setDateEcheance(Date dateEcheance) {
		this.dateEcheance = dateEcheance;
	}
}
