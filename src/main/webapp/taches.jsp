<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Application de Gestion des Tâches</title>
    <!-- Ajoutez des styles CSS ou d'autres dépendances externes ici si nécessaire -->
</head>
<body>

    <h1>Bienvenue dans l'Application de Gestion des Tâches</h1>

    <p>Que souhaitez-vous faire?</p>

    <ul>
        <li><a href="${pageContext.request.contextPath}/tasks">Voir toutes les tâches</a></li>
        <li><a href="${pageContext.request.contextPath}/addTask.jsp">Ajouter une nouvelle tâche</a></li>
    </ul>

    <h2>Liste des Tâches existantes :</h2>

    <table border="1">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Description</th>
                <th>Date d'échéance</th>
                <th>Statut</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="task" items="${tasks}">
                <tr>
                    <td>${task.titre}</td>
                    <td>${task.description}</td>
                    <td>${task.dateEcheance}</td>
                    <td>
                        <form action="${pageContext.request.contextPath}/tasks/edit" method="post">
                            <input type="hidden" name="taskId" value="${task.id}">
                            <input type="radio" name="statut" value="true" ${task.statut ? 'checked' : ''} onchange="this.form.submit()"> Terminer
                            <input type="radio" name="statut" value="false" ${!task.statut ? 'checked' : ''} onchange="this.form.submit()"> En cours
                        </form>
                    </td>
                    <td>
                        <form action="${pageContext.request.contextPath}/tasks/delete" method="post">
                            <input type="hidden" name="taskId" value="${task.id}">
                            <input type="submit" value="Supprimer">
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

</body>
</html>
