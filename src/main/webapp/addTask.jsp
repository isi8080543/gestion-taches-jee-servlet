<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Ajouter une nouvelle tâche</title>

</head>
<body>

<div style="margin: 20px;">
    <h1>Ajouter une nouvelle tâche</h1>

    <!-- Formulaire pour ajouter une nouvelle tâche -->
    <form action="${pageContext.request.contextPath}/tasks/create" method="post">

        <!-- Titre de la tâche -->
        <div style="margin-bottom: 10px;">
            <label for="titre">Titre :</label>
            <input type="text" id="titre" name="titre" required>
        </div>

        <!-- Description de la tâche -->
        <div style="margin-bottom: 10px;">
            <label for="description">Description :</label>
            <textarea id="description" name="description" required></textarea>
        </div>

        <!-- Date d'échéance de la tâche -->
        <div style="margin-bottom: 10px;">
            <label for="dateEcheance">Date d'échéance :</label>
            <input type="date" id="dateEcheance" name="dateEcheance" required>
        </div>

        <!-- Statut de la tâche ( false par defaut)-->

        <!-- Bouton Soumettre -->
        <input type="submit" value="Ajouter la tâche">
    </form>

    <!-- Ajouter tout contenu ou style supplémentaire si nécessaire -->
</div>

</body>
</html>
